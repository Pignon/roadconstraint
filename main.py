import random

import json
import osmnx as ox
import networkx as nx
import plotly.graph_objects as go
import numpy as np

north, east, south, west = 49.159705, -0.399710, 49.211317, -0.331732
# Downloading the map as a graph object
G = ox.graph_from_bbox(north, south, east, west, network_type='drive')


# Plotting the map graph
ox.plot_graph(G)

def plot_path(lat, long, origin_point, destination_point):
    """
    Given a list of latitudes and longitudes, origin
    and destination point, plots a path on a map

    Parameters
    ----------
    lat, long: list of latitudes and longitudes
    origin_point, destination_point: co-ordinates of origin
    and destination
    Returns
    -------
    Nothing. Only shows the map.
    """
    # adding the lines joining the nodes
    fig = go.Figure(go.Scattermapbox(
        name="Path",
        mode="lines",
        lon=long,
        lat=lat,
        marker={'size': 10},
        line=dict(width=4.5, color='blue')))
    # adding source marker
    fig.add_trace(go.Scattermapbox(
        name="Source",
        mode="markers",
        lon=[origin_point[1]],
        lat=[origin_point[0]],
        marker={'size': 12, 'color': "red"}))

    # adding destination marker
    fig.add_trace(go.Scattermapbox(
        name="Destination",
        mode="markers",
        lon=[destination_point[1]],
        lat=[destination_point[0]],
        marker={'size': 12, 'color': 'green'}))

    # getting center for plots:
    lat_center = np.mean(lat)
    long_center = np.mean(long)
    # defining the layout using mapbox_style
    fig.update_layout(mapbox_style="stamen-terrain",
                      mapbox_center_lat=30, mapbox_center_lon=-80)
    fig.update_layout(margin={"r": 0, "t": 0, "l": 0, "b": 0},
                      mapbox={
                          'center': {'lat': lat_center,
                                     'lon': long_center},
                          'zoom': 13})
    fig.show()


def euclidian(x, y, x1, y1):
    return (x - x1) * (x - x1) + (y - y1) * (y - y1)


def getClosestNode(G, x, y):
    min_dist = 1000000000000
    res_elem = 0
    b = (x, y)
    for elem in G.nodes:
        a = (G.nodes.get(elem)['y'], G.nodes.get(elem)['x'])
        if euclidian(a[0], a[1], b[0], b[1]) < min_dist:
            min_dist = euclidian(a[0], a[1], b[0], b[1])
            res_elem = elem

    return res_elem


def getClosestStreetPath(G, fx, fy, gx, gy):
    origin_node = getClosestNode(G, fx, fy)
    destination_node = getClosestNode(G, gx, gy)

    route = nx.shortest_path(G, origin_node, destination_node, weight='length')

    res = []
    for y in route:
        point = G.nodes[y]
        res.append((point['x'], point['y']))

    return res


with open('resources/path.json', 'r') as f:
    data = json.load(f)
    paths = {"Paths": []}

    long = []
    lat = []

    for i in range(0, len(data["Coord"])):

        y = (i+1)%len(data["Coord"])
        path = getClosestStreetPath(G, float(data["Coord"][i]['x']),
                                    float(data["Coord"][i]['y']),
                                    float(data["Coord"][y]['x']),
                                    float(data["Coord"][y]['y'])
                                    )

        for t in path:
            long.append(t[0])
            lat.append(t[1])


        jsonpath = {
            "from": data["Coord"][i]['name'],
            "to": data["Coord"][y]['name'],
            "path": path
        }
        paths["Paths"].append(jsonpath)

    op = (float(data["Coord"][0]['x']),
          float(data["Coord"][0]['y']))
    dt = (float(data["Coord"][-1]['x']),
          float(data["Coord"][-1]['y']))

    plot_path(lat, long, op, dt)

with open('results/truck.json', 'w') as json_file:
    json.dump(paths, json_file)

with open('resources/datas.json', 'r') as f:
    data = json.load(f)
    jsonres = "{"
    for qrt in data["Coord"]:
        x = float(qrt['x'])
        y = float(qrt['y'])
        mag = int(qrt['mag'])
        l = []
        for i in range(0, 10):
            l.append(
                (x + random.randint(-5, 5) / 3000,
                 y + random.randint(-5, 5) / 3000)
            )
        dst = data["CoordMagasins"][mag]
        res = []
        for elem in l:
            path = getClosestStreetPath(G, elem[0],
                                        elem[1],
                                        float(dst['x']),
                                        float(dst['y'])
                                        )
            res.append(path)
        jsonpath = {
            qrt['name']: res
        }
        jsonres += str(jsonpath).split("{")[1].split("}")[0].replace("(", "[").replace(")", "]").replace("\'",
                                                                                                         "\"") + ","

    jsonres = jsonres[:-1] + "}"

with open("results/peoples.json", "w") as f:
    f.write(jsonres)
