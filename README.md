# RoadConstraint

With given coordinates, gives the shortest path respecting the constraints of the road to go from point A to point B.


## Installation & Start

```
python -m pip install requirements.txt
python main.py
```

We recommand to open the whole project with Pycharm